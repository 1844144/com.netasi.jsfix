// Example of using jsfix
// {
//   hide_section : [
//     '[id^=price_45_]',
//     '[id^=price_48_]',
//   ],
//   if_selected_show : {
//     parent: '[name=price_49]',
//     groups: [
//     { 
//       trigger: 'input[value=234], input[value=247] ',
//       elements: '[id^=price_45_]',
//     },
//     {
//       trigger: 'input[value=248], input[value=249]',
//       elements: '[id^=price_48_]'
//     }
//     ],
//   },
// 	disable_input : [
// 		'input#1',
// 		'input#2',
// 	],
//  payment_on_last_page: true,

// }
// 
// { payment_on_last_page: true,
//   hide_section : [
   
//   ],
//   if_selected_show : {
//     parent: '[name=price_49]',
//     groups: [
//     { 
//       trigger: 'input[value=234], input[value=247] ',
//       elements: '[id^=price_45_]',
//     },
//     {
//       trigger: 'input[value=248], input[value=249]',
//       elements: '[id^=price_48_]'
//     }
//     ],
//   },
//   disable_input : [
//     'input#first_name',
//     'input#last_name',
//   ],

// }
// 
// 
// 

// payment_on_last_page: true

// conditional:
//  - if_selected : 234,247
//    show_section: 45
//  - if_selected: 248,249
//    show_section: 48
//    show_element: custom_30
// page:
//  - if_first: true
//    hide_element: custom_33
//    hide_price_item: 250
// disable_input : 
//  - first_name
//  - last_name

// handler: #the_css_input_selector
//  
//  - if_selected: #css_selector, #css_selecor
//    page: first
//    page: not first
//    page: any
//    hide : #css_selector  !!uncheck that you hide
// 
//	  show : 
//       price_item: #css_selector
// 	     price_set: #selector
//       custom_field: #selector
//       manual_selector: #selector
//    other_action : price_item, price_set, 

// current ONLY FOR AMENSDA

function process_amen(first_time){
	cur_val = cj('[name=price_9]:checked').val();
		option = cj('#price_6_25').parent();
		if (cur_val == 41 ) option.show();
		else {
			option.hide();
		}
		if (first_time != true)	if ( cj('#price_6_25').is(':checked') ) cj('#price_6_25').click()  ; 
}
cj (function(){
	process_amen(true);
	// amen adjusting
	cj('#CIVICRM_QFID_0_30').click();
	cj('[name=price_4]').closest('.crm-section').hide();
	cj('[name=price_9]').change(process_amen);
});





function with_elem(elem_str, what_to_do){
	arr = elem_str.split(',');
	out = [];
	for (i=0; i< arr.length; i++){
		out.push('[name='+arr[i]+']');
	}
	out = out.join(',');
	elem = cj(out).closest('.crm-section');
	if (what_to_do =='show') {
		elem.show();
	}
	if (what_to_do == 'hide'){
		elem.hide();
	}
}
function hide_price_item(elem_str){
	elem_str=elem_str.toString();
	cj.each( elem_str.split(','), function(idx, elem){
		cj('[value='+elem+']').closest('.price-set-row').hide();
	});
}

cj(function(){
	if ('hide_section' in window.jsfix_config) {
		cj.each( window.jsfix_config.hide_section, function (ind, selector){
			cj(selector).closest('.crm-section').hide();
		} );
	}

	if ('conditional' in window.jsfix_config) {
		el = cj('[value='+window.jsfix_config.conditional[0].if_selected.split(',')[0]+']');
			parent_selector = "[name="+el.attr('name')+"]";
		function process_groups(){
			
			var cur_val = cj(parent_selector+':checked').val();

			cj.each( window.jsfix_config.conditional, function (ind, cnf){
				var info = cnf.if_selected.split(',');
				section_selector = '[name^=price_'+cnf.show_section+']';

				if ( cj.inArray(cur_val , info) != -1) {
					if ('show_element' in cnf) with_elem(cnf.show_element,'show');

					
					cj(section_selector).closest('.crm-section').show();
				}
				else {
					if ('show_element' in cnf) with_elem(cnf.show_element,'hide');
					cj(section_selector).parent().find(':checked').click();
					cj(section_selector).closest('.crm-section').hide();
				}
			});
			
		};

		cj(parent_selector).on('change', process_groups);
		process_groups();
	}
	if ('page' in window.jsfix_config){
		cj.each(window.jsfix_config.page, function(ind,elem){
			
			if ( ( cj('#additional_participants').length != 0) == (elem.if_first==true)) {
					if ('hide_element' in elem) {
						with_elem(elem.hide_element, 'hide');
					}
					if ('hide_price_item' in elem) hide_price_item(elem.hide_price_item);
			}
		});

	}

	if ('disable_input' in window.jsfix_config) {
		logged_in = false;

		
		
		cj.each( window.jsfix_config.disable_input, function (ind, selector){
			if ( cj('#crm-event-register-different').length != 0) {
				selector = '#'+selector
				cj(selector).on('focus',function(){ 
					cj(this).blur();
					href = cj('#crm-event-register-different a').attr('href');
					if (cj(this).attr('flag') != "1")
					{
						cj(this).after("<span class='warn' style='color:red; margin-left: 10px'>Your name cannot be modified. You may register a different person <a href='"+href+"'>here</a>	");
						cj(this).attr('flag','1');

					}
					 	});

			}
		} );
	}



});