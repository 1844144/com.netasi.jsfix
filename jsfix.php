<?php

require_once 'jsfix.civix.php';

/**
 * Implementation of hook_civicrm_config
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function jsfix_civicrm_config(&$config) {
  _jsfix_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function jsfix_civicrm_xmlMenu(&$files) {
  _jsfix_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function jsfix_civicrm_install() {
  _jsfix_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function jsfix_civicrm_uninstall() {
  _jsfix_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function jsfix_civicrm_enable() {
  _jsfix_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function jsfix_civicrm_disable() {
  _jsfix_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function jsfix_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _jsfix_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function jsfix_civicrm_managed(&$entities) {
  _jsfix_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_caseTypes
 *
 * Generate a list of case-types
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function jsfix_civicrm_caseTypes(&$caseTypes) {
  _jsfix_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implementation of hook_civicrm_alterSettingsFolders
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function jsfix_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _jsfix_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}



function jsfix_civicrm_buildForm( $formName, &$form ){
  // Membership form id is 1

  // error_log($formName);
  if ( startsWith($formName, 'CRM_Event_Form_Registration_') ) {
    $eid = $form->_eventId;
    
    $result = civicrm_api3('CustomField', 'get', array(
      'sequential' => 1,
      'name' => "jsfix_config",
    ));

    if (!empty($result['values'])) {
      
      $cf_id = $result['values'][0]['id'];

      $field_name = "custom_".$cf_id;

      $result = civicrm_api3('Event', 'get', array(
        'sequential' => 1,
        'return' => $field_name,
        'id' => $form->_eventId,
      ));
      $the_script = $result['values'][0][$field_name];

      require_once "Spyc.php";

      $the_config = Spyc::YAMLLoadString($the_script);


      // var_dump(strpos($the_script,'payment_on_last_page'));
      // check for plp and include scripts if we need

      if ( array_key_exists('payment_on_last_page', $the_config) && $the_config['payment_on_last_page']==True ) {

        CRM_Core_Resources::singleton()->addScriptFile('com.netasi.jsfix', 'jquery.form.min.js');
        CRM_Core_Resources::singleton()->addScriptFile('com.netasi.jsfix', 'plp.js');
      }


      if ($formName == 'CRM_Event_Form_Registration_Register' || $formName == 'CRM_Event_Form_Registration_AdditionalParticipant') {
          $json = json_encode($the_config);
          // include event config
          echo "<script> window.jsfix_config = JSON.parse('$json'); </script>";
          // as this is Membership form we will disable 
          // ability to change Mem Categories
          CRM_Core_Resources::singleton()->addScriptFile('com.netasi.jsfix', 'jsfix.js');
          // $form->_paymentFields['bank_account_number']['is_required'] = false;
        }
      }
  }

}

function jsfix_civicrm_alterContent(  &$content, $context, $tplName, &$object ){
    // yes ugly hack to hide this field
    $pageName = $object->getVar('_name');
    // if (get_class($object) == 'CRM_Event_Form_Registration_Register'){
    //   $content = str_replace('[civicrm component','----',$content);
    // };
    if ($pageName == 'CRM_Event_Page_EventInfo'){
      $content = str_replace('id="jsfix__','style="display:none" id="jsfix__',$content);
    }
}