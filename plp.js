	// if there is plp_status
	// 
	// statuses are
	// first, participant, billing, order

pname = cj('input[name=qfKey]').val();
ppart = pname+'p';

BEFORE_BILLING = 0;
AFTER_BILLING = 1;
function whitescreen(){
cj('body').append('<div id="whitescreen" style="width:100%;height:100%;position:fixed;left:0;top:0;z-index:1000000;text-align:center;font-size:38px;background:white;padding-top:3cm;color:lightgray;">Thinking...</div>');
	
}
function whitescreen_off(){
	cj('#whitescreen').hide();
}
whitescreen();
cj(document).ready(function(){


	
	begin = window.location.pathname;
	link_reg = begin+'?page=CiviCRM&q=civicrm/event/register&_qf_Register_display=true&qfKey='+pname;
	link_confirm = begin+'?page=CiviCRM&q=civicrm/event/register&_qf_Confirm_display=true&qfKey='+pname;

	plink = pname+'link';
	pconfirm = pname+'confirm';
	if (!(pname in localStorage)) {
		localStorage[pname] = BEFORE_BILLING;
	}


	plp_status = localStorage[pname];
	page = cj('form').attr('class'); 

	if (plp_status ==BEFORE_BILLING){
		if ( page=='CRM_Event_Form_Registration_Register'){
			// hide billing
			cj('.crm-group.payment_options-group').hide();
			cj('.crm-group.payment_options-group').css('position','fixed');
			cj('.crm-group.payment_options-group').css('top:','-1000px');
			cj('.crm-group.payment_options-group').css('left:','-1000px');
			cj('#billing-payment-block').hide();
			cj('#CIVICRM_QFID_0_payment_processor').click();
			// and remember participant

			cj('#additional_participants').change(function(){
				localStorage[ppart] = parseInt(cj( "#additional_participants option:selected" ).text()) - 1;
			});


			
		} 
		else if (page == 'CRM_Event_Form_Registration_Confirm' ) {
			// save confirm link
			localStorage[pconfirm] = window.location.href;
			// change status and jump
			localStorage[pname] = AFTER_BILLING;
			go_to(link_reg);
		}

	} 
	else if (plp_status == AFTER_BILLING) {
		if (page=='CRM_Event_Form_Registration_Register') 
		{
			make_billing_page();
		}
		else if (page == 'CRM_Event_Form_Registration_Confirm')
		{	
			// edit back link
			cj('input.crm-form-submit.cancel').click(function(e){
				e.preventDefault();
				go_to(link_reg);
			});
		}
		else 
		{
			// jump to confirm
			go_to(link_confirm);

		}
	}

	// handling of Next and Prev
	// cj(cj('form')[0]).submit(function(e){
	// 	if (plp_status =='first_page'){
	// 			set_plp('waiting_confirm');
	// 	}
	// 		// e.preventDefault();
	// 		// alert('catch');
		
	// });
	whitescreen_off();
});


function make_billing_page(){
	
	localStorage['plp-title'] = cj('h1').text();
	cj('h1').text('Payment');

	cj('fieldset.crm-profile').hide();
	cj('fieldset#priceset').hide();
	cj('table.form-layout-compressed').hide();
	cj('#crm-event-register-different').hide();
	cj('.crm-section.additional_participants-section').hide();



	// add back button
	cj('.crm-button.crm-button-type-upload.crm-button_qf_Register_upload').parent().prepend('<span class="crm-button crm-button-type-1 crm-button_qf_Participant_1_back crm-icon-button"> <span class="crm-button-icon ui-icon-triangle-1-w"> </span><input class="crm-form-submit cancel" crm-icon="triangle-1-w" name="my_back" value="Go Back" type="submit" id="my_back"></input></span></span>');
	cj('input#my_back').click(function(e){
		e.preventDefault();
		localStorage[pname] = BEFORE_BILLING;
		go_to( get_pre_confirm_link() );
	});
	
	// pay before options
	if ( cj('#CIVICRM_QFID_0_payment_processor').is(':checked') )  {
		cj('#CIVICRM_QFID_13_payment_processor').click();
	}
	cj('#CIVICRM_QFID_0_payment_processor, #CIVICRM_QFID_0_payment_processor+label ').hide();




}

function go_to(href){
	whitescreen();
	window.location = href;
}

function get_pre_confirm_link(){
	if (localStorage[ppart] == 0){
		return window.location.href;
	}
	else {
		pname = cj('input[name=qfKey]').val();
		begin = window.location.pathname;
		link_reg = begin+'?page=CiviCRM&q=civicrm/event/register&_qf_Participant_'+localStorage[ppart]+'_display=true&qfKey='+pname;
		return link_reg;
	} 
}

